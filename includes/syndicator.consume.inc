<?php

/**
 * @file
 *   PubSubHubbub consuming
 */

/**
 * Implements hook_form_alter().
 */
function syndicator_consume_form_alter(&$form, $form_state, $form_id) {
  // Add consumed status to the node form
  $node_type = isset($form['type']) && isset($form['type']['#value']) ? $form['type']['#value'] : '';
  if (!empty($node_type)
     && $node_type  . '_node_form' == $form_id
     && syndicator_consumed($form['#node'])
  ) {
    $default = $form['#node']->syndicator_consume_status;
    if (user_access('administer nodes')) {
      // Add ability to manually override consumption status for admins
      $form['syndicator_consume_status'] = array(
        '#type' => 'fieldset',
        '#title' => t('Retrieved status'),
      );
      $form['syndicator_consume_status']['syndicator_consume_status'] = array(
        '#type' => 'radios',
        '#options' => array(
          SYNDICATOR_CONSUMED_NEW => 'New',
          SYNDICATOR_CONSUMED_ACCEPTED => 'Accepted',
          SYNDICATOR_CONSUMED_REJECTED => 'Rejected',
        ),
        '#default_value' => $default,
      );
    }
    else {
      // Otherwise just pass through the current status
      $form['syndicator_consume_status'] = array(
        '#type' => 'hidden',
        '#value' => $default,
      );
    }
  }
}

/**
 * Implements hook_feeds_atom_rdf_map_alter().
 */
function syndicator_consume_feeds_atom_rdf_map_alter(&$target_item, $source_item, $source) {
  // Tell ourselves this node is being updated from the feed
  // We do this because we have to do actions AFTER the mapping takes place
  $target_item->consumption_in_progress = TRUE;
}


/**
 * @defgroup consume_node
 * @{
 */

/**
 * Implements hook_node_presave() from Drupal 7.
 */
function syndicator_consume_node_presave(&$node) {
  if (syndicator_consumed($node)) {
    if (isset($node->consumption_in_progress) && $node->consumption_in_progress) {
      if (syndicator_consume_is_modified($node->nid)) {
        // Ensure changed values are retained when consumed nodes are updated
        // During the consumed node update process, the entire value of the node
        // is replaced with the version from our subscription.  In this case we are
        // replacing the values we had previously modified back to those values.
        // This is being done so late in the process just in case there have been
        // other modifications to the node after our hook is called during mapping
        module_load_include('inc', 'syndicator', 'includes/syndicator.consume.crud');
        $last_merge = syndicator_consume_get_last_merged_version($node->nid);
        $ours = syndicator_consume_get_our_version($node->nid);
        $theirs = drupal_clone($node);
        $node = syndicator_merge($last_merge, $ours, $theirs, 'ours');

        // Set their version of the node, since alters to it will stop here
        // Alters to the version we're keeping isn't done yet, so we'll set that
        // version in hook_node_update().
        syndicator_consume_set_versions($node->nid, $node, $theirs);
      }
    }
  }
}

/**
 * Implements hook_node_insert() from Drupal 7.
 */
function syndicator_consume_node_insert(&$node) {
  // Set config for newly consumed nodes
  if (isset($node->consumption_in_progress) && $node->consumption_in_progress) {
    module_load_include('inc', 'syndicator', 'includes/syndicator.consume.crud');

    // Set the first consume status
    syndicator_consume_set_status($node->nid, SYNDICATOR_CONSUMED_NEW);

    // Ensure the node is owned by the consumer user
    $node->uid = variable_get('syndicator_consume_uid', 1);

    // Set all the versions of the node
    syndicator_consume_set_versions($node->nid, $node, $node, $node);
  }
}

/**
 * Implements hook_node_update() from Drupal 7.
 */
function syndicator_consume_node_update(&$node) {
  if (syndicator_consumed($node)) {
    module_load_include('inc', 'syndicator', 'includes/syndicator.consume.crud');

    // Update the consume status
    if (isset($node->syndicator_consume_status)) {
      syndicator_consume_set_status($node->nid, $node->syndicator_consume_status);
    }

    if (isset($node->consumption_in_progress) && $node->consumption_in_progress) {
      if (syndicator_consume_is_modified($node->nid)) {
        // Something weird is happening where the mapped version of the node is
        // being applied *again* sometime after presave and before update
        // Merge in again
        $last_merge = syndicator_consume_get_last_merged_version($node->nid);
        $ours = syndicator_consume_get_our_version($node->nid);
        $theirs = drupal_clone($node);
        $node = syndicator_merge($last_merge, $ours, $theirs, 'ours');

        // Now that all the alters have taken place, we'll update our version
        syndicator_consume_set_our_version($node);
      }
      else {
        // If the consumed node hasn't been modified yet, update the "last merged"
        // state of the node.
        syndicator_consume_set_versions($node->nid, $node, $node, $node);
      }
    }
    else {
      // Mark that the consumed node has been updated different from the feed
      syndicator_consume_set_modified($node->nid);

      // And update our stored version
      syndicator_consume_set_our_version($node);
    }
  }
}

/**
 * Implements hook_node_load() from Drupal 7.
 */
function syndicator_consume_node_load(&$node) {
  if (syndicator_consumed($node)) {
    module_load_include('inc', 'syndicator', 'includes/syndicator.consume.crud');

    // Load the node's consumption status
    $node->syndicator_consume_status = syndicator_consume_get_status($node->nid);
    // Load the node's modification status
    $node->syndicator_consume_is_modified = syndicator_consume_is_modified($node->nid);
  }
}

/**
 * Implements hook_node_delete() from Drupal 7.
 */
function syndicator_consume_node_delete(&$node) {
  db_query("DELETE FROM {syndicator_consumed} WHERE nid = %d", $node->nid);
}

/**
 * @}
 */

/**
 * Is the current node an original, or consumed from elsewhere
 *
 * @param $node
 *   Node object
 * @return
 *   Bool
 */
function syndicator_consumed($node) {
  // If the node objects has a consume status, then this is a consumed node
  if (isset($node->syndicator_consume_status) && is_numeric($node->syndicator_consume_status)) {
    return TRUE;
  }

  // Check to see if if there is a consume status in the database
  if (isset($node->nid) && $node->nid > 0) {
    module_load_include('inc', 'syndicator', 'includes/syndicator.consume.crud');
    $current_status = syndicator_consume_get_status($node->nid);
    if (!is_null($current_status)) {
      return TRUE;
    }
  }

  // Doesn't look like this is a consumed node!
  return FALSE;
}

/**
 * Performs a 3-way merge of the given items
 *
 * @param $last_merge
 *   An array or object that reflects the original or "last merged" version
 * @param $ours
 *   An array or object that reflects the current version being used
 * @param $theirs
 *   An array or object that you wish to merge into the set
 * @param $version
 *   If there is a conflict, what version should be taken.  Accepted values are
 *   'ours' and 'theirs'.  If NULL then conflicts will be displayed by returning
 *   the conflicted key as an array of 'ours' and 'theirs' values
 * @return
 *   Outputs a merged version in the same format as $last_merge
 */
function syndicator_merge($last_merge, $ours, $theirs, $version = NULL) {
  module_load_include('inc', 'syndicator', 'includes/syndicator.consume.crud');

  if (is_object($last_merge)) $last_merge = drupal_clone($last_merge);
  if (is_object($ours)) $ours = drupal_clone($ours);
  if (is_object($theirs)) $theirs = drupal_clone($theirs);

  // If the values we've received are objects or arrays, we need to recursively
  // dive into the values and diff each section
  if (is_array($last_merge) || is_object($last_merge)) {
    // Prepare the storaged var in the desired format
    $merge = is_array($last_merge) ? array() : new stdClass;

    // Figure out the value for all the options in the $last_mergeinal
    foreach ($last_merge as $key => $value) {
      // Recursively build up the theirs value
      $ours_value = syndicator_get_value($ours, $key);
      $theirs_value = syndicator_get_value($theirs, $key);
      $merge_value = syndicator_merge($value, $ours_value, $theirs_value, $version);
      $merge = syndicator_add_value($merge, $key, $merge_value);

      // Remove processed keys
      syndicator_unset_key($ours, $key);
      syndicator_unset_key($theirs, $key);
    }

    // The $oursrent value may contain options not included in the $last_mergeinal
    if (is_array($ours) || is_object($ours)) {
      foreach ($ours as $key => $value) {
        $theirs_value = syndicator_get_value($theirs, $key);
        if ($version) {
          $merge_value = $version == 'ours' ? $value : $theirs_value;
        }
        else {
          $merge_value = array(
            'ours' => $value,
            'theirs' => $theirs_value,
          );
        }
        $merge = syndicator_add_value($merge, $key, $merge_value);
        syndicator_unset_key($ours, $key);
        syndicator_unset_key($theirs, $key);
      }
    }

    // Same as the $theirs value, find options not in $last_mergeinal or $oursrent
    // Fortunately this is now the dregs and we don't have anything left over to
    // compare with
    if (is_array($theirs) || is_object($theirs)) {
      foreach ($theirs as $key => $value) {
        $merge = syndicator_add_value($merge, $key, $value);
      }
    }
  }

  // Otherwise our task is a lot easier and we can just return the desired value
  else {
    $ours_changed = !($last_merge == $ours);
    $theirs_changed = !($last_merge == $theirs);

    if ($ours_changed && $theirs_changed) {
      switch ($version) {
        case 'ours':
          $merge = $ours;
          break;
        case 'theirs':
          $merge = $theirs;
          break;
        default:
          $merge = array(
            'ours' => $ours,
            'theirs' => $theirs,
          );
      }
    }
    elseif ($ours_changed && !$theirs_changed) {
      $merge = $ours;
    }
    elseif (!$ours_changed && $theirs_changed) {
      $merge = $theirs;
    }
    else {
      $merge = $last_merge;
    }
  }

  // Yay, we're done.
  return $merge;
}
