<?php

/**
 * @file
 *   Feeds configuration
 */

/**
 * Implements hook_feeds_importer_default().
 */
function syndicator_feeds_importer_default() {
  $export = array();
  $uid =  variable_get('syndicator_consume_uid', 1);

  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'syndicator_consumer';
  $feeds_importer->config = array(
    'name' => 'PuSH Consumer',
    'description' => 'Consumer of Atom+RDF feeds',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 1,
        'designated_hub' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsAtomRDFParser',
      'config' => array(),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsAtomRDFProcessor',
      'config' => array(
        'input_format' => '0',
        'update_existing' => '2',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'url',
            'target' => 'url',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'guid',
            'target' => 'guid',
            'unique' => 1,
          ),
        ),
        'author' => $uid,
        'authorize' => 0,
        'content_type' => 'article',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '1800',
    'expire_period' => 3600,
    'import_on_create' => 1,
  );
  $export['syndicator_consumer'] = $feeds_importer;

  return $export;
}
