<?php

/**
 * @file
 *   Setting up access control and associated permissions
 */

/**
 * Restrict editing fields of syndicated nodes to only privileged users.
 */
function syndicator_permissions_permission(&$permissions) {
  $permissions[] = 'edit syndicated content'; // Administrators can edit any field
  $permissions[] = 'edit only alowed fields in syndicated content'; // Jane and Rebecca can edit the node body
}

/**
 * Implementation of hook_menu().
 */
function syndicator_permissions_menu(&$items) {
  $items['admin/content/syndicator/permissions'] = array(
    'title' => 'Permissions',
    'description' => 'Permissions options for PubSubHubbub content syndication.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('syndicator_permissions_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'includes/syndicator.permissions.inc',
    'weight' => 1,
    'type' => MENU_LOCAL_TASK,
  );
}

/**
 * Configuration options for syndicated content
 */
function syndicator_permissions_settings_form() {
  $form = array();
  $syndication_node_types = variable_get('syndicator_publish_node_types', array());
  $redirect = TRUE;

  $syndication_permissions = variable_get('syndicator_permissions', array());

  foreach ($syndication_node_types as $key => $type_name) {
    if ($key !== $type_name) {
      continue;
    }
    $redirect = FALSE;

    $fields = get_type_fields($type_name);

    $form['fieldset_' . $type_name] = array(
      '#type' => 'fieldset',
      '#title' => check_plain(t('!type_name', array('!type_name' => $type_name))),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $groups = array();
    if (module_exists('fieldgroup')) {
      $groups = fieldgroup_groups($type_name);
      foreach ($groups as $group) {
        $grouped_fields = $fields[$group['group_name']];
        unset($fields[$group['group_name']]);
        $form['fieldset_' . $type_name][$group['group_name']] = array(
          '#type' => 'fieldset',
          '#title' => t('%group_label', array('%group_label' => $group['label'])),
          '#attributes' => array('class' => 'grouped'),
        );
        $form['fieldset_' . $type_name][$group['group_name']][$type_name . '-' . $group['group_name']] = array(
          '#description' => t('Choose fields for content type "%type_name"', array('%type_name' => $type_name)),
          '#type' => 'checkboxes',
          '#default_value' => (!empty($syndication_permissions[$type_name . '-' . $group['group_name']]))
                              ? $syndication_permissions[$type_name . '-' . $group['group_name']]
                              : array(),
          '#options' => $grouped_fields,
        );
      }
    }

    $form['fieldset_' . $type_name][$type_name] = array(
      '#description' => t('Choose fields for content type "%type_name"', array('%type_name' => $type_name)),
      '#type' => 'checkboxes',
      '#default_value' => (!empty($syndication_permissions[$type_name]))
                          ? $syndication_permissions[$type_name]
                          : array(),
      '#options' => $fields,
    );

  }

  if ($redirect) {
    drupal_set_message(t('Please select content types to syndicate!'), 'warning');
    drupal_goto('admin/content/syndicator');
  }
  drupal_add_css(drupal_get_path('module', 'syndicator') . '/assets/syndicator.permissions.css');
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['#theme'] = 'system_settings_form';
  $form['#submit'][] = 'syndicator_permissions_settings_form_submit';

  return $form;
}

/**
 * Save configuration options for syndicated content
 */
function syndicator_permissions_settings_form_submit($form, &$form_state) {
  $syndication_node_types = variable_get('syndicator_publish_node_types', array());

  $syndicator_permissions = array();
  foreach ($syndication_node_types as $key => $type_name) {
    if ($key !== $type_name) {
      continue;
    }
    $syndicator_permissions[$type_name] = $form_state['values'][$type_name];
    if (module_exists('fieldgroup')) {
      $groups = fieldgroup_groups($type_name);
      foreach ($groups as $group) {
        $syndicator_permissions[$type_name . '-' . $group['group_name']] = $form_state['values'][$type_name . '-' . $group['group_name']];
      }
    }

  }
  variable_set('syndicator_permissions', $syndicator_permissions);
  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Implementation of hook_form_alter().
 */
function syndicator_permissions_form_alter(&$form, $form_state, $form_id) {
  global $user;

  if (user_access('edit syndicated content')) {
    return TRUE;
  }

  $syndication_node_types = variable_get('syndicator_publish_node_types', array());
  if (isset($form['type'])
     && isset($form['#node'])
     && $form['type']['#value'] . '_node_form' == $form_id
     && syndicator_consumed($form['#node'])
     && isset($syndication_node_types[$form['type']['#value']])
     && $syndication_node_types[$form['type']['#value']] === $form['type']['#value']
  ) {

    if (!user_access('edit only alowed fields in syndicated content')) {
      drupal_access_denied();
      exit;
    }

    disable_field($form);

    drupal_add_js(drupal_get_path('module', 'syndicator') . '/assets/jquery.blockUI.js');
    drupal_add_js(drupal_get_path('module', 'syndicator') . '/assets/syndicator.permissions.js');
    drupal_add_css(drupal_get_path('module', 'syndicator') . '/assets/syndicator.permissions.css');

  }
}

/**
 * Disable form field
 */
function disable_field(&$form) {
  $syndication_permissions = variable_get('syndicator_permissions', array());

  foreach ($form as $key => $field) {
    if (substr($key, 0, 1) !== '#'
         && (!isset($syndication_permissions[$form['type']['#value']][$key])
          || $syndication_permissions[$form['type']['#value']][$key] !== $key)
         && $key !== 'buttons'
    ) {
      if (isset($syndication_permissions[$form['type']['#value'] . '-' . $key])) {
        foreach ($field as $g_key => $g_field) {
          if (substr($g_key, 0, 1) !== '#'
               && (!isset($syndication_permissions[$form['type']['#value'] . '-' . $key][$g_key])
                || $syndication_permissions[$form['type']['#value'] . '-' . $key][$g_key] !== $g_key)
          ) {
            $form[$key][$g_key]['#prefix'] .= '<div class="disabled-field">';
            $form[$key][$g_key]['#suffix'] =  '</div>' . $form[$key][$g_key]['#suffix'];
          }
        }
      }
      else {
        $form[$key]['#prefix'] .= '<div class="disabled-field">';
        $form[$key]['#suffix'] =  '</div>' . $form[$key]['#suffix'];
      }
    }
  }
}

/**
 * Returns array of fields for content type
 */
function get_type_fields($type_name) {
  $result = array();
  $type = content_types($type_name);
  $fields = $type['fields'];
  $extras = $type['extra'];

  foreach ($fields as $name => $field) {
    if ($field['display_settings']['parent']) {
      $result[$field['display_settings']['parent']][$name] = $field['widget']['label'];
    }
    else {
      $result[$name] = $field['widget']['label'];
    }
  }
  foreach ($extras as $name => $field) {
    $result[$name] = $field['label'];
  }
  return $result;
}
