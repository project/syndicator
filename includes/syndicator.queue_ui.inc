<?php

/**
 * @file
 *   User interface for managing the syndication queue
 */

/**
 * Implementation of hook_menu().
 */
function syndicator_queue_ui_menu(&$items) {
  // AJAX callbacks
  $items['syndicator/accept/%node'] = array(
    'page callback' => 'syndicator_schedule_node',
    'page arguments' => array(2),
    'access callback' => 'node_access',
    'access arguments' => array('update', 2),
    'file' => 'includes/syndicator.queue_ui.inc',
    'type' => MENU_CALLBACK,
  );
  $items['syndicator/reject/%node'] = array(
    'page callback' => 'syndicator_reject_node',
    'page arguments' => array(2),
    'access callback' => 'node_access',
    'access arguments' => array('update', 2),
    'file' => 'includes/syndicator.queue_ui.inc',
    'type' => MENU_CALLBACK,
  );
  $items['syndicator/revert/%node'] = array(
    'page callback' => 'syndicator_revert_node',
    'page arguments' => array(2),
    'access callback' => 'node_access',
    'access arguments' => array('update', 2),
    'file' => 'includes/syndicator.queue_ui.inc',
    'type' => MENU_CALLBACK,
  );
}

/**
 * Save schedule form via AJAX
 */
function syndicator_schedule_node($node) {
  $data = $_POST;
    syndicator_consume_set_status($node->nid, SYNDICATOR_CONSUMED_ACCEPTED);
    // @TODO: Make sure node is published

  if (!empty($data)) {
    $node->workflow = $data['workflow'];
    $node->workflow_scheduled = $data['workflow_scheduled'];
    $node->workflow_scheduled_date = $data['workflow_scheduled_date'];
    $node->workflow_scheduled_hour = $data['workflow_scheduled_hour'];
    $node->workflow_comment = '';

    node_save($node);

    $workflow_published_states = variable_get('syndicator_publish_workflow_published_states', array());
    if (in_array($node->workflow, $workflow_published_states)) {
      drupal_json_output(array(
        'status' => 'true',
        'message' => 'Saved successfully!',
      ));
    }
    else {
      drupal_json_output(array(
        'status' => 'false',
        'message' => 'Not published!',
      ));
    }

  }
  else {
    drupal_json(array(
      'status' => 'false',
      'message' => 'Data is empty!',
    ));
  }
}

/**
 * Reject syndication via AJAX
 */
function syndicator_reject_node($node) {
  // @TODO: Unpublish node
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    syndicator_consume_set_status($node->nid, SYNDICATOR_CONSUMED_REJECTED);
    drupal_json(array(
      'status' => 'true',
      'message' => 'Saved successfully!',
    ));
  }
  else {
    drupal_json(array(
      'status' => 'false',
      'message' => 'Method not allowed',
    ));
  }
}

/**
 * Revert syndication to New via AJAX
 */
function syndicator_revert_node($node) {
  // @TODO: Set workflow back to default
  // @TODO: What is the default workflow?
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    syndicator_consume_set_status($node->nid, SYNDICATOR_CONSUMED_NEW);
    drupal_json(array(
      'status' => 'true',
      'message' => 'Saved successfully!',
    ));
  }
  else {
    drupal_json(array(
      'status' => 'false',
      'message' => 'Method not allowed',
    ));
  }
}
