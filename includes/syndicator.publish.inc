<?php

/**
 * @file
 *   PubSubHubbub publishing
 */

define('SYNDICATOR_PUBLISH_NO', 0);
define('SYNDICATOR_PUBLISH_YES', 1);


/**
 * @defgroup admin_pages Administration pages
 * @{
 * Administrative pages to configure syndicator settings
 */

/**
 * Implementation of hook_menu().
 */
function syndicator_publish_menu(&$items) {
  $items['admin/content/syndicator'] = array(
    'title' => 'Syndicator',
    'description' => 'Configuration options for PubSubHubbub content syndication.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('syndicator_publish_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'includes/syndicator.publish.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/content/syndicator/general'] = array(
    'title' => 'General',
    'description' => 'Configuration options for PubSubHubbub content syndication.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('syndicator_publish_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'includes/syndicator.publish.inc',
    'weight' => 0,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
}

/**
 * FAPI configuration options for publishing content
 */
function syndicator_publish_settings_form() {
  $form = array();

  $form['syndicator_notify_immediately'] = array(
    '#title' => t('When should we notify subscribers?'),
    '#type' => 'radios',
    '#default_value' => variable_get('syndicator_notify_immediately', 0),
    '#options' => array(
      t('During cron'),
      t('Immediately'),
    ),
  );

  $form['syndicator_publish_node_types'] = array(
    '#title' => t('Content types to syndicate'),
    '#description' => t('Choose which content types subscribers should have access to consume.'),
    '#type' => 'checkboxes',
    '#default_value' => variable_get('syndicator_publish_node_types', array()),
    '#options' => node_get_types('names'),
  );

  $form['syndicator_publish_workflow_published_states'] = array(
    '#title' => t('Workflow published states'),
    '#description' => t('Choose workflow published state which can be shared.'),
    '#type' => 'checkboxes',
    '#default_value' => variable_get('syndicator_publish_workflow_published_states', array()),
    '#options' => workflow_get_states(),
  );

  return system_settings_form($form);
}

/**
 * Implementation of hook_form_alter().
 */
function syndicator_publish_form_alter(&$form, $form_state, $form_id) {
  $syndication_node_types = variable_get('syndicator_publish_node_types', array());
  $node_type = isset($form['type']) && isset($form['type']['#value']) ? $form['type']['#value'] : '';
  if (!empty($node_type)
     && $node_type  . '_node_form' == $form_id
     && isset($syndication_node_types[$node_type])
     && $syndication_node_types[$node_type] === $node_type
     && !syndicator_consumed($form['#node'])
  ) {
    // Add syndication checkbox
    if (isset($form['#node']->nid)) {
      $default =  syndicator_publish_get_status($form['#node']->nid);
    }
    elseif ('author' == $node_type) {
      $default = SYNDICATOR_PUBLISH_YES;
    }
    else {
      $default =  SYNDICATOR_PUBLISH_NO;
    }
    $form['syndicator_publish'] = array(
      '#title' => t('Syndicate content'),
      '#type' => 'checkbox',
      '#default_value' => $default,
    );
  }
}

/**
 * @}
 */


/**
 * @defgroup publish_node
 * @{
 */

/**
 * Implementation of hook_node_insert() from Drupal 7.
 */
function syndicator_publish_node_insert(&$node) {
  // Notify subscribers that a new node is available
  if (syndicator_published($node)) {
    syndicator_notify_subscribers($node->nid);
  }

  // Save the node's publish status
  if (isset($node->syndicator_publish)) {
    syndicator_publish_set_status($node->nid, $node->syndicator_publish);
  }
}

/**
 * Implementation of hook_node_update() from Drupal 7.
 */
function syndicator_publish_node_update(&$node) {
  // Do the same thing that happens in node_insert
  syndicator_publish_node_insert($node);
}

/**
 * Implementation of hook_node_load() from Drupal 7.
 */
function syndicator_publish_node_load(&$node) {
  // Load the node's publish status
  $shared_node_types = variable_get('syndicator_publish_node_types', array());
  if (in_array($node->type, $shared_node_types)) {
    $node->syndicator_publish = syndicator_publish_get_status($node->nid);
  }
}

/**
 * @}
 */


/**
 * @defgroup ping_subscribers Pinging
 * @{
 */

/**
 * Send a notification to all subscribers.
 */
function syndicator_notify_subscribers($nid) {
  $notify = variable_get('syndicator_notify_immediately', 0);
  $feed_url = url('syndication/feed.xml', array('absolute' => TRUE));
  $changed = syndicator_get_node_xml($nid);

  // http_build_url doesn't exist on all systems
  if (!function_exists('http_build_url')) {
    module_load_include('inc', 'syndicator', 'includes/http_build_url');
  }

  // Queue up notifications
  // We're not using push_hub_notify() because it doesn't handle duplicates

  // Subscribers are found by who is subscribed to a given feed -- including the
  // www or lack thereof.  We're going to collect from both versions.
  $feed_url_parsed = parse_url($feed_url);
  $feed_urls = array($feed_url);
  if (substr($feed_url_parsed['host'], 0, 4) == 'www.') {
    $feed_url_parsed['host'] = str_replace('www.', '', $feed_url_parsed['host']);
  }
  else {
    $feed_url_parsed['host'] = 'www.' . $feed_url_parsed['host'];
  }
  $feed_urls[] = http_build_url('', $feed_url_parsed);
  $subscriptions = array();
  foreach ($feed_urls as $url) {
    $subscriptions += push_hub()->allSubscribers($url);
  }

  // Gather current notifications
  $sql = "SELECT * FROM {queue} WHERE name = 'push_hub_notifications'";
  $result = db_query($sql);
  $queue = array();
  while ($row = db_fetch_object($result)) {
    $row->data = (array) unserialize($row->data);
    foreach ($feed_urls as $topic) {
      if ($row->data['topic'] == $topic) {
        $queue[$row->data['subscriber']] = $row;
      }
    }
  }

  $info = new stdClass;
  $info->data = array(
    'topic' => $feed_url,
    'changed' => $changed,
  );
  $operations = array();
  foreach ($subscriptions as $subscription) {
    $info->data['subscriber'] = $subscription;

    if ($notify) {
      // Unfortunately, I cannot figure out how to avoid duplicates while also
      // ensuring the 'changed' XML is the latest version
      $operations[] = array('push_hub_notify_subscriber_batchapi', array($info->data));
    }
    else {
      // Check to see if we've queued this guy for notification
      if (isset($queue[$subscription])) {
        // Update the notification with new info
        if (isset($queue[$subscription]->item_id)) {
          $row = $queue[$subscription];
          $row->data = $info->data;
          drupal_write_record('queue', $row, 'item_id');
        }
      }
      else {
        $queue[$subscription] = $info;
        push_hub_queue()->createItem($info->data);
      }
    }
  }

  // Send out notifications if config says to notify immediately
  $batch = array(
    'title' => t('Notifying subscribers'),
    'operations' => $operations,
  );
  if ($notify) {
    batch_set($batch);
  }
}

/**
 * Gathers an XML/Atom version of the given node
 */
function syndicator_get_node_xml($nid) {
  node_load($nid, NULL, TRUE); // Clear cache
  return views_embed_view('syndicator_publish', 'feed_2', $nid);
}

/**
 * @}
 */


/**
 * @defgroup syndication_api API
 * @{
 */

/**
 * Verify that a node should be syndicated
 */
function syndicator_published($node) {
  $syndication_node_types = array_filter(variable_get('syndicator_publish_node_types', array()));
  $workflow_published_states = array_filter(variable_get('syndicator_publish_workflow_published_states', array()));

  // Node type exists in the variable 'syndicator_publish_node_types'
  // Status is published
  // Workflow is published
  // 'Share content' checkbox is checked
  $workflow_state = isset($node->workflow) ? $node->workflow : (isset($node->_workflow) ? $node->_workflow : FALSE);
  if (isset($syndication_node_types[$node->type])
       && $syndication_node_types[$node->type] === $node->type
       && $node->status == 1
       && (
        (is_numeric($workflow_state) && in_array($workflow_state, $workflow_published_states))
        || !$workflow_state
      )
       && isset($node->syndicator_publish)
       && $node->syndicator_publish == SYNDICATOR_PUBLISH_YES
    ) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Retreive whether the node should be syndicated, regardless if it is published
 */
function syndicator_publish_get_status($nid) {
  $sql = "SELECT status " .
      "FROM {syndicator_published} " .
      "WHERE nid = %d " .
      "LIMIT 1";
  $status = db_result(db_query($sql, $nid));
  if ($status == SYNDICATOR_PUBLISH_YES) {
    return SYNDICATOR_PUBLISH_YES;
  }
  return SYNDICATOR_PUBLISH_NO;
}

/**
 * Set whether or not the node should be syndicated when it is published
 */
function syndicator_publish_set_status($nid, $status) {
  if (!is_numeric($nid) || !is_numeric($status)) {
    return FALSE;
  }

  $exists = db_result(db_query("SELECT nid FROM {syndicator_published} WHERE nid = %d", $nid));
  $row = new stdClass;
  $row->nid = $nid;
  $row->status = $status;
  if ($exists) {
    $result = drupal_write_record('syndicator_published', $row, array('nid'));
  }
  else {
    $result = drupal_write_record('syndicator_published', $row);
  }

  // Allow modules to hook into setting status
  // Invokes hook_syndicator_publish_set_status($nid, $status)
  module_invoke_all('syndicator_publish_set_status', $nid, $status);

  return $result;
}

/**
 * @}
 */
