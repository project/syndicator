<?php

/**
 * @file
 *   Compatibility layer to allow D7 API usage on a D6 site
 */

/**
 * Implements hook_nodeapi().
 *
 * Invokes D7 API:
 *   -  hook_node_delete()
 *   -  hook_node_insert()
 *   -  hook_node_load()
 *   -  hook_node_prepare()
 *   -  hook_node_presave()
 *   -  hook_node_revision_delete()
 *   -  hook_node_search_result()
 *   -  hook_node_update()
 *   -  hook_node_update_index()
 *   -  hook_node_validate()
 *   -  hook_node_view()
 */
function syndicator_nodeapi(&$node, $op) {
  $search = array(
    'delete revision',
    ' ',
  );
  $replace = array(
    'revision_delete',
    '_',
  );

  $hook = 'node_' . str_replace($search, $replace, $op);
  module_invoke('syndicator', $hook, $node);
}

/**
 * Implements hook_perm().
 */
function syndicator_perm() {
  return syndicator_permission();
}
