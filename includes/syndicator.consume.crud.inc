<?php

/**
 * @file
 *   CRUD events for syndicator consumption
 */

/**
 * @defgroup consume_status Get/Set consumption status
 * @{
 */

define('SYNDICATOR_CONSUMED_NEW', 0);
define('SYNDICATOR_CONSUMED_ACCEPTED', 1);
define('SYNDICATOR_CONSUMED_REJECTED', 2);

/**
 * Retreive the current status of the consumed node's usage
 *
 * @param $nid
 *   Node id of the node to check
 * @return
 *   The numeric status of the node if it has a status, and NULL if no status
 *   exists for the given node
 */
function syndicator_consume_get_status($nid) {
  $sql = "SELECT status " .
      "FROM {syndicator_consumed} " .
      "WHERE nid = %d";
  $status = db_result(db_query($sql, $nid));
  if (is_numeric($status)) {
    return $status;
  }
  return NULL;
}

/**
 * Set the current status of the consumed node's usage
 *
 * @param $nid
 *   Node id of the node to set
 * @param $status
 *   The   status to set.  Choose one of SYNDICATOR_CONSUMED_NEW,
 *   SYNDICATOR_CONSUMED_ACCEPTED,  or SYNDICATOR_CONSUMED_REJECTED
 * @return
 *   bool
 *   Whether or not the status was set correctly
 */
function syndicator_consume_set_status($nid, $status) {
  if (!is_numeric($status)) {
    return FALSE;
  }

  $row = db_fetch_object(db_query("SELECT * FROM {syndicator_consumed} WHERE nid = %d", $nid));
  $row->status = $status;
  if (isset($row->nid)) {
    $result = drupal_write_record('syndicator_consumed', $row, 'nid');
  }
  else {
    $row->nid = $nid;
    $result = drupal_write_record('syndicator_consumed', $row);
  }

  // Allow modules to hook into setting status
  // Invokes hook_syndicator_consume_set_status($nid, $new_status)
  module_invoke_all('syndicator_consume_set_status', $nid, $status);

  return $result;
}

/**
 * @}
 */


/**
 * @defgroup consume_modified Set if the node has been modified
 * @{
 */

define('SYNDICATOR_CONSUMED_NOT_MODIFIED', 0);
define('SYNDICATOR_CONSUMED_MODIFIED', 1);

/**
 * Retreive the modified status of the given node
 *
 * @param $nid
 *   Node id of the node to check
 * @return
 *   bool
 */
function syndicator_consume_is_modified($nid) {
  $sql = "SELECT modified FROM {syndicator_consumed} WHERE nid = %d";
  $result = db_result(db_query($sql, $nid));
  if ($result > 0) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Set that a given consumed modified node has been modified
 *
 * @param $nid
 *   Node id of the node
 * @return
 *   Bool value if the save was successful
 */
function syndicator_consume_set_modified($nid) {
  return syndicator_consume_set_modify_status($nid, SYNDICATOR_CONSUMED_MODIFIED);
}

/**
 * Unset the modified status of a node
 *
 * @param $nid
 *   Node id of the node
 * @return
 *   Bool value if the save was successful
 */
function syndicator_consume_set_unmodified($nid) {
  return syndicator_consume_set_modify_status($nid, SYNDICATOR_CONSUMED_NOT_MODIFIED);
}

/**
 * Set a modification status on a node
 *
 * @param $nid
 *   Node id of the node to store
 * @return
 *   Bool value if the save was successful
 */
function syndicator_consume_set_modify_status($nid, $status) {
  $row = db_fetch_object(db_query("SELECT * FROM {syndicator_consumed} WHERE nid = %d", $nid));
  $row->modified = $status;
  if (isset($row->nid)) {
    return drupal_write_record('syndicator_consumed', $row, 'nid');
  }
  $row->nid = $nid;
  return drupal_write_record('syndicator_consumed', $row);
}

/**
 * @}
 */


/**
 * @defgroup consume_version Get/Set/Remove consumed versions of nodes
 * @{
 */

/**
 * Retreive our current version of the given node
 *
 * Wrapper for syndicator_consume_get_version().
 *
 * @param $nid
 *   Node id of the node you want to retreive
 * @return
 *   object of our version of the node
 */
function syndicator_consume_get_our_version($nid) {
  return syndicator_consume_get_version($nid, 'ours');
}

/**
 * Retreive their current version of the given node
 *
 * Wrapper for syndicator_consume_get_version().
 *
 * @param $nid
 *   Node id of the node you want to retreive
 * @return
 *   object of their version of the node
 */
function syndicator_consume_get_their_version($nid) {
  return syndicator_consume_get_version($nid, 'theirs');
}

/**
 * Retreive the last merge version of the given node
 *
 * Wrapper for syndicator_consume_get_version().
 *
 * @param $nid
 *   Node id of the node you want to retreive
 * @return
 *   object of last merged version of the node
 */
function syndicator_consume_get_last_merged_version($nid) {
  return syndicator_consume_get_version($nid, 'last_merge');
}

/**
 * Retreive our current version of the given node
 *
 * Wrapper for syndicator_consume_get_version().
 *
 * @param $nid
 *   Node id of the node you want to retreive
 * @param $version
 *   The version you wish to acheive.  It is the desired column name for the
 *   table   {syndicator_consumed}.  Accepted values are 'ours', 'theirs',
 *   'last_merge'
 * @return
 *   object of the desired version of the node
 */
function syndicator_consume_get_version($nid, $version) {
  $sql = "SELECT %s FROM {syndicator_consumed} WHERE nid = %d";
  $result = db_result(db_query($sql, $version, $nid));
  return unserialize($result);
}

/**
 * Store our current version of the node
 *
 * @param $node
 *   Node object to store
 * @return
 *   Bool value if the save was successful
 */
function syndicator_consume_set_our_version($node) {
  return syndicator_consume_set_versions($node->nid, $node);
}

/**
 * Store their current version of the node
 *
 * @param $node
 *   Node object to store
 * @return
 *   Bool value if the save was successful
 */
function syndicator_consume_set_their_version($node) {
  return syndicator_consume_set_versions($node->nid, NULL, $node);
}

/**
 * Store the last merged version of the node
 *
 * @param $node
 *   Node object to store
 * @return
 *   Bool value if the save was successful
 */
function syndicator_consume_set_last_merged_version($node) {
  return syndicator_consume_set_versions($node->nid, NULL, NULL, $node);
}

/**
 * Set multiple different versions simultaneously
 *
 * @param $nid
 *   Node id of the node we're working with
 * @param $ours
 *   Node object of our current version
 * @param $theirs
 *   Node object of their current version
 * @param $last_merge
 *   Node object when ours and their version were identical
 * @return
 *   Bool value if the save was successful
 */
function syndicator_consume_set_versions($nid, $ours = NULL, $theirs = NULL, $last_merge = NULL) {
  if (empty($ours) && empty($theirs) && empty($last_merge)) {
    // Dude, you didn't give me anything to do
    return FALSE;
  }
  $row = db_fetch_object(db_query("SELECT * FROM {syndicator_consumed} WHERE nid = %d", $nid));
  $row->ours = empty($ours) ? $row->ours : serialize($ours);
  $row->theirs = empty($theirs) ? $row->theirs : serialize($theirs);
  $row->last_merge = empty($last_merge) ? $row->last_merge : serialize($last_merge);
  if (isset($row->nid)) {
    return drupal_write_record('syndicator_consumed', $row, 'nid');
  }
  $row->nid = $nid;
  return drupal_write_record('syndicator_consumed', $row);
}

/**
 * Remove the original version of the node
 *
 * @param $nid
 *   Node id of the node to remove
 * @return
 *   Bool value if the removal was successful
 */
function syndicator_consume_remove_version($nid, $version) {
  return db_query("UPDATE {syndicator_consumed} SET %s = NULL WHERE nid = %d", $version, $nid);
}

/**
 * @}
 */


/**
 * @defgroup manipulate_values Add/Get/Unset values from array or object
 * @{
 */

/**
 * Add a value to a given key regardless if the item is an array or object
 *
 * @param $item
 *   An array or object that you wish to add a keyed value to
 * @param $key
 *   The desired name of the key
 * @param $value
 *   The desired value to set at the given key
 * @param $override
 *   If the key already exists, should we override it with the given value
 * @return
 *   The item you provided, but with the key and value added to it
 */
function syndicator_add_value($item, $key, $value, $override = FALSE) {
  if (is_array($item) && ($override || !isset($item[$key]))) {
    $item[$key] = $value;
  }
  elseif (is_object($item) && ($override || !isset($item->$key))) {
    $item->$key = $value;
  }
  return $item;
}

/**
 * Get a value from a given key regardless if the item is an array or object
 *
 * @param $item
 *   An array or object that you wish to retreive a value from
 * @param $key
 *   The desired name of the key
 * @return
 *   The value of the given key
 */
function syndicator_get_value($item, $key) {
  if (is_array($item) && isset($item[$key])) {
    return $item[$key];
  }
  elseif (is_object($item) && isset($item->$key)) {
    return $item->$key;
  }
  return NULL;
}

/**
 * Unset a given key regardless if the item is an array or object
 *
 * @param $item
 *   An array or object that you wish to modify
 * @param $key
 *   The desired name of the key
 */
function syndicator_unset_key(&$item, $key) {
  if (is_array($item) && isset($item[$key])) {
    unset($item[$key]);
  }
  elseif (is_object($item) && isset($item->$key)) {
    unset($item->$key);
  }
}

/**
 * @}
 */
