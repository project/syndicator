<?php

/**
 * @file
 *   Views configuration
 */

/**
 * Implements hook_views_handlers().
 */
function syndicator_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'syndicator') . '/includes/views/handlers',
    ),
    'handlers' => array(
      // Field that will allow in-line editing of the workflow state and set
      // the node's syndication state to 'accepted'
      'syndicator_handler_field_schedule_node' => array(
        'parent' => 'views_handler_field_node_link',
      ),
      // Field that will set the node's syndication state to 'accepted' in-line
      'syndicator_handler_field_reject_node' => array(
        'parent' => 'views_handler_field_node_link',
      ),
      // Field to revert a node
      'syndicator_handler_field_revert_node' => array(
        'parent' => 'views_handler_field_node_link',
      ),
      // Field to display a user's path
      'syndicator_handler_field_user_path' => array(
        'parent' => 'views_handler_field_node_path',
      ),
      // Filter to show node that can be syndicated
      'syndicator_handler_filter_syndicatable' => array(
        'parent' => 'views_handler_filter_boolean_operator',
      ),
      // Filter for a node's consume status
      'syndicator_handler_filter_retrieved_status' => array(
        'parent' => 'views_handler_filter_numeric',
      ),
    ),
  );
}

/**
 * Implements hook_views_data().
 */
function syndicator_views_data() {
  $data = array();

  // Filter for if a node is allowed to be syndicated
  $data['syndicator_published']['table']['group']  = t('Syndicator');
  $data['syndicator_published']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  $data['syndicator_published']['status'] = array(
    'title' => t('Available for syndication'),
    'help' => t('Displays if node is published and configured to be syndicated'),
    'filter' => array(
      'handler' => 'syndicator_handler_filter_syndicatable',
      'label' => t('Available for syndication'),
      'type' => 'yes-no',
      'use equal' => TRUE,
    ),
  );

  // Filter for the node's current retreived status
  $data['syndicator_consumed']['table']['group']  = t('Syndicator');
  $data['syndicator_consumed']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['syndicator_consumed']['status'] = array(
    'title' => t('Retrieved status'),
    'help' => t('Syndication retrieved status'),
    'filter' => array(
      'handler' => 'syndicator_handler_filter_retrieved_status',
      'label' => t('Retrieved status'),
      'allow empty' => TRUE,
    ),
  );

  // Field to mark a node's retreive status as "accepted" and inline workflow
  $data['node']['syndicator_schedule'] = array(
    'title' => t('Syndication schedule'),
    'help' => t('Syndication field.'),
    'field' => array(
      'handler' => 'syndicator_handler_field_schedule_node',
    ),
  );

  // Field to mark a node's retreive status as "rejected"
  $data['node']['syndicator_reject'] = array(
    'title' => t('Syndication reject'),
    'help' => t('Syndication field'),
    'field' => array(
      'handler' => 'syndicator_handler_field_reject_node',
    ),
  );

  // Field to revert a node
  $data['node']['syndicator_revert'] = array(
    'title' => t('Syndication revert'),
    'help' => t('Syndication field'),
    'field' => array(
      'handler' => 'syndicator_handler_field_revert_node',
    ),
  );

  // Field to revert a node
  $data['node']['syndicator_user_path'] = array(
    'title' => t('User path'),
    'help' => t('Path to the node user\'s profile'),
    'field' => array(
      'handler' => 'syndicator_handler_field_user_path',
    ),
  );

  return $data;
}
