<?php

/**
 * @file
 *   Filter for content that can be syndicated
 */

class syndicator_handler_filter_syndicatable extends views_handler_filter_boolean_operator {
  function query() {
    $syndication_node_types = array_filter(variable_get('syndicator_publish_node_types', array()));
    $workflow_published_states = array_filter(variable_get('syndicator_publish_workflow_published_states', array()));

    $this->ensure_my_table();
    $n = 'node';
    $osp = 'syndicator_published';
    $wn = $this->query->add_table('workflow_node');

    if ($this->value == 1) {
      // Published query
      $this->query->add_where($this->options['group'],
        $n . '.status = 1');
      $this->query->add_where($this->options['group'],
        $osp . '.status = 1');
      if (!empty($syndication_node_types)) {
        $this->query->add_where($this->options['group'],
          $n . '.type IN (' . db_placeholders($syndication_node_types, 'text') . ')',
          $syndication_node_types);
      }
      if (!empty($workflow_published_states)) {
        $this->query->add_where($this->options['group'],
          $wn . '.sid IN (' . db_placeholders($workflow_published_states, 'int') . ') ' .
              'OR ' . $wn . '.sid IS NULL',
          $workflow_published_states);
      }
    }
    else {
      // Non-published query
      $where = '';
      if (!empty($syndication_node_types)) {
        $where .= "{$n_alias}.type IN (" . db_placeholders($syndication_node_types, 'text') . ") " .
            "AND ( ";
      }
      $where .= "{$n_alias}.status != 1 " .
          "OR {$osp_alias}.status != 1 " .
          "OR {$wn_alias}.sid NOT IN (" . db_placeholders($workflow_published_states, 'int') . ") ";
      if (!empty($syndication_node_types)) {
        $where .= ")";
      }
    }
  }
}
