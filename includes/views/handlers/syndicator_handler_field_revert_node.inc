<?php

/**
 * @file
 * Contains the handler for reverting syndicated nodes in-line
 */

/**
 * Field handler to revert the syndicated node in-line
 */
class syndicator_handler_field_revert_node extends views_handler_field_node_link {
  function render($values) {
    drupal_add_js(drupal_get_path('module', 'syndicator') . '/assets/syndicator.queue_ui.js');
    $nid = $values->{$this->aliases['nid']};
    return l(t('Revert'), 'syndicator/revert/' . $nid);
  }
}
