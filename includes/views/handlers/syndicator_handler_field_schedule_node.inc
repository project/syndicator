<?php

/**
 * @file
 * Contains the handler for accepting syndicated nodes in-line
 */

/**
 * Field handler to accept the syndicated node in-line
 */
class syndicator_handler_field_schedule_node extends views_handler_field_node_link {
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function render($values) {
    $nid = $values->{$this->aliases['nid']};
    $node = node_load($nid);
    $form_html = drupal_get_form('syndicator_workflow_form', $node);
    return $form_html;
  }

}

function syndicator_workflow_form(&$form_state, $node) {
  module_load_include('inc', 'syndicator', 'includes/syndicator.consume');
  module_load_include('inc', 'syndicator', 'includes/syndicator.consume.crud');

  drupal_add_css(drupal_get_path('module', 'syndicator') . '/assets/syndicator.queue_ui.css');
  drupal_add_js(drupal_get_path('module', 'syndicator') . '/assets/syndicator.queue_ui.js');

  $choices = workflow_field_choices($node);
  $wid = workflow_get_workflow_for_type($node->type);
  $states = workflow_get_states($wid);
  // If this is a preview, the current state should come from
  // the form values, not the node, as the user may have changed
  // the state.
  $current = isset($form_state['values']['workflow']) ? $form_state['values']['workflow'] : workflow_node_current_state($node);
  $min = $states[$current] == t('(creation)') ? 1 : 2;
  // Stop if user has no new target state(s) to choose.
  if (count($choices) < $min) {
    return;
  }

  $form = array(
    '#action' => '',
    '#attributes' => array('class' => 'syndicator-schedule-form'),
  );

  $workflow = workflow_load($wid);
  $form['#wf'] = $workflow;

  // Check node syndication queue status
  $curent_status = syndicator_consume_get_status($node->nid);
  if ($curent_status && $curent_status == 1) { // Status is "Accepted"
    $name = t('Scheduled');
  }
  else {
    $name = t('Schedule');
  }

  // If the current node state is not one of the choices, autoselect first choice.
  // We know all states in $choices are states that user has permission to
  // go to because workflow_field_choices() has already checked that.
  if (!isset($choices[$current])) {
    $array = array_keys($choices);
    $current = $array[0];
  }

  if (sizeof($choices) > 1) {
    $form['workflow'] = array(
      '#type' => 'fieldset',
      '#title' => check_plain($name),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
  }

  $timestamp = NULL;
  $comment = '';

  // See if scheduling information is present.
  if (isset($node->_workflow_scheduled_timestamp) && isset($node->_workflow_scheduled_sid)) {
    // The default value should be the upcoming sid.
    $current = $node->_workflow_scheduled_sid;
    $timestamp = $node->_workflow_scheduled_timestamp;
    $comment = $node->_workflow_scheduled_comment;
  }

  if (isset($form_state['values']['workflow_comment'])) {
    $comment = $form_state['values']['workflow_comment'];
  }

  // No sense displaying choices if there is only one choice.
  if (sizeof($choices) == 1) {
    $form['workflow'][$name] = array(
      '#type' => 'hidden',
      '#value' => $current,
    );
  }
  else {
    $form['workflow']['nid'] = array(
      '#type' => 'hidden',
      '#value' => $node->nid,
    );

    $title = $form['#wf']->options['name_as_title'] ? $title : '';
    $form['workflow'][$name] = array(
      '#type' => 'radios',
      '#title' => check_plain($title),
      '#options' => $choices,
      '#name' => $name,
      '#parents' => array('workflow'),
      '#default_value' => $current,
    );

    // Display scheduling form only if a node is being edited and user has
    // permission. State change cannot be scheduled at node creation because
    // that leaves the node in the (creation) state.
    $scheduled = $timestamp ? 1 : 0;
    $timestamp = $scheduled ? $timestamp : time();

    $form['workflow']['workflow_scheduled'] = array(
      '#type' => 'radios',
      '#title' => t('Schedule'),
      '#options' => array(
        t('Immediately'),
        t('Schedule for state change at:'),
      ),
      '#default_value' => isset($form_state['values']['workflow_scheduled']) ? $form_state['values']['workflow_scheduled'] : $scheduled,
    );

    $form['workflow']['workflow_scheduled_date'] = array(
      '#type' => 'date',
      '#default_value' => array(
        'day' => isset($form_state['values']['workflow_scheduled_date']['day']) ? $form_state['values']['workflow_scheduled_date']['day'] : format_date($timestamp, 'custom', 'j'),
        'month' => isset($form_state['values']['workflow_scheduled_date']['month']) ? $form_state['values']['workflow_scheduled_date']['month'] : format_date($timestamp, 'custom', 'n'),
        'year' => isset($form_state['values']['workflow_scheduled_date']['year']) ? $form_state['values']['workflow_scheduled_date']['year'] : format_date($timestamp, 'custom', 'Y'),
      ),
    );

    $hours = format_date($timestamp, 'custom', 'H:i');
    $form['workflow']['workflow_scheduled_hour'] = array(
      '#type' => 'textfield',
      '#description' => t('Please enter a time in 24 hour (eg. HH:MM) format. If no time is included, the default will be midnight on the specified date. The current time is: ') . format_date(time()),
      '#default_value' => $scheduled ? (isset($form_state['values']['workflow_scheduled_hour']) ? $form_state['values']['workflow_scheduled_hour'] : $hours) : NULL,
    );

    $form['workflow']['save'] = array(
      '#type' => 'button',
      '#value' => t('Save'),
      '#attributes' => array('class' => 'syndicator-schedule-submit'),
      '#weight' => 5,
    );

  }
  return $form;
}
