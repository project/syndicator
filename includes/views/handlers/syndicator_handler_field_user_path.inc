<?php

/**
 * @file
 *   Field for the user path
 */

/**
 * Views class syndicator_handler_field_user_path
 */
class syndicator_handler_field_user_path extends views_handler_field_node_path {
  function construct() {
    parent::construct();
    $this->additional_fields['uid'] = 'uid';
  }

  function render($values) {
    $uid = $values->{$this->aliases['uid']};
    return url("user/$uid", array('absolute' => $this->options['absolute']));
  }
}
