<?php

/**
 * @file
 * Handler allows to filter syndication retrieved statuses
 */

class syndicator_handler_filter_retrieved_status extends views_handler_filter {

  function construct() {
    if (isset($this->definition['accept null'])) {
      $this->accept_null = (bool) $this->definition['accept null'];
    }
    elseif (isset($this->definition['accept_null'])) {
      $this->accept_null = (bool) $this->definition['accept_null'];
    }
    $this->value_options = NULL;
    parent::construct();
  }

  function get_value_options() {
    $any_label = variable_get('views_exposed_filter_any_label', 'old_any') == 'old_any' ? 'Any' : t('- Any -');
    $this->value_options = array(
      'All' => $any_label,
      SYNDICATOR_CONSUMED_NEW => t('New'),
      SYNDICATOR_CONSUMED_ACCEPTED => t('Accepted'),
      SYNDICATOR_CONSUMED_REJECTED => t('Rejected'),
    );
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['value']['default'] = FALSE;

    return $options;
  }

  function operator_form(&$form, &$form_state) {
    $form['operator'] = array();
  }

  function value_form(&$form, &$form_state) {
    if (empty($this->value_options)) {
      // Initialize the array of possible values for this filter.
      $this->get_value_options();
    }
    if (!empty($form_state['exposed'])) {
      // Exposed filter: use a select box to save space.
      $filter_form_type = 'select';
    }
    else {
      // Configuring a filter: use radios for clarity.
      $filter_form_type = 'radios';
    }
    $form['value'] = array(
      '#type' => $filter_form_type,
      '#title' => check_plain($this->value_value),
      '#options' => $this->value_options,
      '#default_value' => $this->value,
    );
    if (!empty($this->options['exposed'])) {
      $identifier = $this->options['expose']['identifier'];
      if (!isset($form_state['input'][$identifier])) {
        $form_state['input'][$identifier] = $this->value;
      }
      // If we're configuring an exposed filter, add an <Any> option.
      if (empty($form_state['exposed']) || !empty($this->options['optional'])) {
        $any_label = variable_get('views_exposed_filter_any_label', 'old_any') == 'old_any' ? 'Any' : t('- Any -');
        if ($form['value']['#type'] != 'select') {
          $any_label = check_plain($any_label);
        }
        $form['value']['#options'] = array('All' => $any_label) + $form['value']['#options'];
      }
    }
  }

  function value_validate(&$form, &$form_state) {
    if ($this->options['exposed'] && $form_state['values']['options']['value'] == 'All' && empty($form_state['values']['options']['expose']['optional'])) {
      form_set_error('value', t('You must select a value unless this is an optional exposed filter.'));
    }
  }

  function admin_summary() {
    if (!empty($this->options['exposed'])) {
      return t('exposed');
    }
    if (empty($this->value_options)) {
      $this->get_value_options();
    }

    return $this->value_options[$this->value];
  }

  function expose_options() {
    parent::expose_options();
    $this->options['expose']['operator'] = '';
    $this->options['expose']['label'] = $this->value_value;
    $this->options['expose']['optional'] = FALSE;
  }

  function query() {
    $this->ensure_my_table();
    if ($this->value == 'all' || $this->value == array(0 => 'all')) {
      $this->query->add_where($this->options['group'], "$this->table_alias. $this->real_field IS NOT NULL");
    }
    else {
      $this->query->add_where($this->options['group'], "$this->table_alias. $this->real_field " . $this->operator . " %d", $this->value);
    }
  }
}
