<?php

/**
 * @file
 *   Example feed
 */

  print '<?xml version="1.0" encoding="utf-8"?>';
?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <title>Example feed</title>
  <link href="<?php print $feed_url ?>" rel="self" />
  <id><?php print $feed_url ?></id>
  <link rel="hub" href="<?php print $hub_url ?>" />
  <link rel="self" href="<?php print $feed_url ?>" />
  <updated>2012-06-22T16:09:59-04:00</updated>
  <generator uri="<?php print $feed_url ?>">Drupal</generator>

  <entry>
    <title>Foobar Title</title>
    <link href="<?php print $entry_url ?>" />
    <id><?php print $entry_id ?></id>
    <updated>2012-06-22T16:09:59-04:00</updated>
    <published>2012-06-21T16:05:32-04:00</published>
    <author>
      <name>Jane Doe</name>
      <email>jane.doe@example.com</email>
      <uri><?php print $author_url ?></uri>
    </author>
    <content type="text" xml:lang="en">
      <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
        <entity xmlns="http://drupal.org/" type="node" bundle="article">

          <title>Foobar Title</title>
          <properties>
            <nid>32496</nid>
            <type>article</type>
            <uid>14</uid>
            <status>1</status>
            <created>2012-06-21T16:05:32-04:00</created>
            <changed>2012-06-22T16:09:59-04:00</changed>
            <vid>208123</vid>
            <revision_uid>186</revision_uid>
            <title>Foobar Title</title>
            <revision_timestamp>1340395799</revision_timestamp>
            <format>2</format>
            <name>jane.doe</name>
            <path><?php print $path ?></path>
            <last_comment_timestamp>1340201310</last_comment_timestamp>
            <comment_count>142</comment_count>
            <teaser>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas erat velit, sodales eu condimentum sed, pharetra eget nisi. Suspendisse justo quam, accumsan id imperdiet ut, interdum eu ante. Fusce tempus porta mauris sit amet volutpat. Praesent orci nibh, convallis vitae aliquet non, euismod cursus arcu. Duis augue arcu, mattis id.</teaser>
            <body>
              &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce suscipit elit iaculis est lobortis non vestibulum mauris ornare. Donec eget felis eu libero iaculis posuere. Maecenas at justo nec nulla venenatis rhoncus. Vestibulum venenatis, nisi in posuere sollicitudin, magna orci imperdiet metus, ac laoreet dolor velit vitae ante. Vivamus quis est sit amet magna viverra ultricies. Integer at condimentum augue. Curabitur pretium pulvinar dolor gravida luctus. Praesent lacinia sem dui. Praesent nec pretium erat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fringilla neque vestibulum leo mattis et ullamcorper magna aliquam. Fusce orci turpis, aliquet sed volutpat sed, lacinia id massa.&lt;/p&gt;
              &lt;p&gt;Nullam in felis at risus porta convallis quis id massa. Suspendisse eu suscipit leo. Nam euismod arcu ut lacus sagittis rhoncus. Nam sagittis ante eu magna bibendum venenatis pulvinar augue laoreet. Curabitur orci felis, pellentesque at posuere a, consequat at eros. Nunc vehicula libero a lectus facilisis pretium porttitor urna ultricies. Phasellus nunc urna, mattis nec tincidunt eget, sollicitudin non ipsum.&lt;/p&gt;
            </body>
          </properties>

          <taxonomy>
            <term name="Relationships">
              <link><?php print $site_path .'/taxonomy/term/338' ?></link>
              <label>Relationships</label>
              <title>Relationships</title>
              <subject>Relationships</subject>
              <description>Nullam in felis at risus porta convallis quis id massa</description>
              <vocabulary>Parent topic</vocabulary>
            </term>
            <term name="career">
              <link><?php print $site_path .'/taxonomy/term/1556' ?></link>
              <label>career</label>
              <title>career</title>
              <subject>career</subject>
              <description>Consectetur adipiscing elit</description>
              <vocabulary>Topics</vocabulary>
            </term>
            <term name="family">
              <link><?php print $site_path .'/taxonomy/term/1556' ?></link>
              <label>family</label>
              <title>family</title>
              <subject>family</subject>
              <description>Lorem ipsum dolor sit amet</description>
              <vocabulary>Topics</vocabulary>
            </term>
          </taxonomy>

          <field type="text" name="field_singlevalue">
            <field-instance>
              <column name="value">Lorem</column>
            </field-instance>
          </field>

          <field type="text" name="field_multiplevalue">
            <field-instance>
              <column name="value">Ipsum</column>
            </field-instance>
            <field-instance>
              <column name="value">Dolor</column>
            </field-instance>
          </field>

        </entity>
      </rdf:RDF>
    </content>
  </entry>

</feed>
