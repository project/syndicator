<?php

/**
 * @file
 *   Presentation logic
 */

/**
 * Implements template_syndicator_simpletest_feed_preprocess().
 */
function syndicator_preprocess_syndicator_example_feed(&$vars) {
  $path = 'syndicator/example';
  $vars['path'] = $path;
  $vars['feed_url'] = url($path . '.xml', array('absolute' => TRUE));
  $vars['entry_url'] = url($path, array('absolute' => TRUE));
  $vars['entry_id'] = url($path . '/id', array('absolute' => TRUE));
  $vars['author_url'] = url($path . '/author', array('absolute' => TRUE));
  $vars['hub_url'] = url('pubsubhubbub/endpoint', array('absolute' => TRUE));
  $vars['site_path'] = url('', array('absolute' => TRUE));
}
