
SYNDICATOR

This module assembles the push_hub+feeds_atom+views_atom stack and adds extra
goodies along the way, namely a moderation queue for newly consumed content,
access control for modifying consumed content, and merging changes of the
original content with changes of your local version.  Overall, the goal of this
module is to make it quicker and easier to syndicate content via PubSubHubbub.

Learn more about PubSubHubbub: http://en.wikipedia.org/wiki/PubSubHubbub


CONFIGURATION

Configuration settings are located at 'admin/content/syndicator'.  The following
is an explaination of the options.

# When should we notify subscribers?

Syndicator uses the PubSubHubBub protocol and its goal is to notify subscribers
of new content and modified content as quickly as possible.  Your choices are:

  * 'Immediately' selection will bring the user to a processing bar after saving
    a node as it runs through all the subscribers and sends them the
    notification about the content.  The subscribing site will get the
    notification as quickly as it takes to run through this process.  This is
    very helpful during development, but isn't advisable in production.

  * 'During cron' won't interupt the workflow of your users and will offload the
    notification during the cron processes.  If you have A LOT of subscribers,
    this will help slowly move through the notification queue so it does not
    overload your server.  You can process the notification queue at a faster
    rate than Drupal cron by updating your crontab to run "drush queue-cron"
    every minute or so.


# Content types to syndicate

Choose which content types are allowed to be syndicated.  Any content type that
you check here will have the _ability_ to be syndicated.  Any content type
checked here will have a checkbox added to the node form titled, "Syndicate
content".  When a node has the "Syndicate content" field checked and is
published, it will be included in your syndication feed.

TODO: Choose which fields of content types can be included when syndicated.
      Currently, ALL data about the node is shared.


# Workflow published states

The goal is for your syndication feed to include only published content.  We
have added integration with the Workflow module so you can be sure that
published content has a workflow state you deem to be public.  Check all states
whose content can be included in the syndication feed.  A node must be
$node->state published, have one of the workflow states you select here (or not
associated with a workflow), and have the "Syndicate content" field selected.

TODO: Make workflow integration optional.  Currently, it could be buggy without
      the workflow module.


PUBLISHING CONTENT

The syndicator_publish view will automatically create a PubSubHubbub-enabled
feed powered by the views_atom module.  Along with the entire node object, it
includes the following configurable items and default values:

  * Title        - $node->title
  * Summary      - $node->summary
  * URL          - url($node->path, array('absolute' => TRUE))
  * Updated      - $node->changed
  * Published    - $node->created
  * Author       - $user->name
  * Author Email - $user->mail
  * Author URL   - url('user'. $user->uid, array('absolute' => TRUE))

You can change these values by adding the desired values as fields to the view
display, and then configuring the display style.

The default feed locations are:

  * syndicator/feed.xml
  * syndicator/node/%node/feed.xml

The output is an atom envelope with a RDF package (ultimate goal being 100%
RDF).  Anyone using the feeds_atom module will be able to consume these feeds.
If the consumer is PubSubHubbub enabled, they will automatically subscribe to
your site and be pinged on updates.

You can access an example of how the feed would output with content here:
  * syndicator/example.xml


CONSUMING CONTENT

You can add new subscriptions to atom+rdf, PubSub-enabled feeds at
  * feeds/import/syndicator_consumer

After subscribing to a feed, you will automatically react to pings and consume
new and updated content.

Consumption assumes you are consuming like-to-like content types.  The
assumption is that the entity structure will be identical on the publishing and
consuming site.

Currently it tries to merge on a field-by-field basis your version and the new
version if you have modified a consumed node.  The merging process needs to grow
to be more sophisticated.

TODO: A better UI for managing subscriptions

TODO: Allow mapping to loosen the like-to-like dependency


MANAGING CONSUMED CONTENT

You can moderate consumed content at 'admin/content/syndicator/queue'.  Newly
consumed items will be in the "new" queue.  From there, you can "accept" the
content by scheduling a workflow transition, or "reject" the item and get it out
of your way.  You can access all of your "accepted" and "rejected" items from
their respective queues.  You are able to change your mind and reject accepted
items, or accept previously rejected items.

TODO: Configuration to set default value of incoming content

TODO: Remove workflow dependency


DRUSH

If you are not careful with your development environments, you can publish
content to production by mistake!  Drush tools have been provided to help you
sanitize your database for development use.  Lets say you are managing two sites
that share content -- siteA and siteB.  The following are examples you would run
on the siteA site when in a development environment.

  * drush subscriber-switch siteB.com siteB.dev
  * drush subscription-switch siteB.com siteB.dev
  * drush topic-switch siteA.dev


THE FUTURE

I would love to keep growing this module.  Send feature requests and
co-contributor requests my way!

