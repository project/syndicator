<?php

/**
 * @file
 *   Drush commands for syndicator
 */

/**
 * Implements hook_drush_command().
 */
function syndicator_drush_command() {
  $items = array();

  $items['subscriber-switch'] = array(
    'description' => 'Change the hostname of a subscriber.',
    'arguments' => array(
      'subscriber' => 'The hostname of the subscriber you want to change.',
      'hostname' => 'The hostname you want to change the subscriber to.',
    ),
    'examples' => array(
      'drush subscriber-switch www.example.com sub.example.org' => 'Change all ' .
          'subscriber references from www.example.com to sub.example.org.',
    ),
  );
  $items['subscription-switch'] = array(
    'description' => 'Change the hostname of our subscription.',
    'arguments' => array(
      'subscription' => 'The hostname of the subscription you want to change.',
      'hostname' => 'The hostname you want to change the subscription to.',
    ),
    'examples' => array(
      'drush subscription-switch www.example.com sub.example.org' => 'Change all ' .
          'subscription references from www.example.com to sub.example.org',
    ),
  );
  $items['topic-switch'] = array(
    'description' => 'Change the hostnames of our topics to the given hostname.',
    'arguments' => array(
      'hostname' => 'The hostname you want to change the topics to.',
    ),
    'examples' => array(
      'drush topic-switch sub.example.org' => 'Change all ' .
          'of our topic references to sub.example.org',
    ),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function syndicator_drush_help($section) {
  switch ($section) {
    case 'meta:syndicator:title':
      return dt('Syndicator commands');
    case 'meta:syndicator:summary':
      return dt('Drush tools for managing subscriptions.');
    case 'drush:subscriber-switch':
      return dt('Change the hostname of a subscriber.');
    case 'drush:subscription-switch':
      return dt('Change the hostname of our subscriptions.');
    case 'drush:topic-switch':
      return dt('Change the hostname of our topics.');
  }
}


/**
 * @defgroup validation
 * @{
 */

/**
 * Implementation of drush_hook_COMMAND_validate() for subscriber-switch.
 */
function drush_syndicator_subscriber_switch_validate() {
  $args = func_get_args();
  return drush_orion_syndiation_validate_args($args);
}

/**
 * Implementation of drush_hook_COMMAND_validate() for subscription-switch.
 */
function drush_syndicator_subscription_switch_validate() {
  $args = func_get_args();
  return drush_orion_syndiation_validate_args($args);
}

/**
 * Implementation of drush_hook_COMMAND_validate() for topic-switch.
 */
function drush_syndicator_topic_switch_validate() {
  $args = func_get_args();
  if (!isset($args[0]) || (isset($args[0]) && empty($args[0]))) {
    $errors = TRUE;
    drush_print('You have not provided a valid hostname to switch to.');
    drush_print('It should be in the format of "sub.example.com"');
    return drush_set_error('DRUSH_SUBSCRIBER_SWITCH_ERROR', dt('Arguments supplied are incorrect'));
  }
}

/**
 * Checks supplied $args to ensure subscription and hostname is set correctly
 */
function drush_orion_syndiation_validate_args($args) {
  $errors = FALSE;
  if (!isset($args[0]) || (isset($args[0]) && empty($args[0]))) {
    $errors = TRUE;
    drush_print('You have not provided a valid hostname for a subscriber.');
    drush_print('Subscriber should be in the format of "sub.example.com"');
  }
  if (!isset($args[1]) || (isset($args[1]) && empty($args[1]))) {
    $errors = TRUE;
    drush_print('You have not provided a valid hostname to switch to.');
    drush_print('It should be in the format of "sub.example.com"');
  }
  if ($errors) {
    return drush_set_error('DRUSH_SUBSCRIBER_SWITCH_ERROR', dt('Arguments supplied are incorrect'));
  }
}

/**
 * @}
 */


/**
 * @defgroup commands
 * @{
 */

/**
 * Implementation of drush_hook_COMMAND for subscriber-switch.
 */
function drush_syndicator_subscriber_switch($subscriber, $hostname) {
  $successful = TRUE;

  // Change push_hub_subscriptions
  $sql_select = "SELECT * FROM {push_hub_subscriptions} " .
      "WHERE subscriber LIKE '%%%s%%'";
  $sql_update = "UPDATE {push_hub_subscriptions} " .
      "SET subscriber = '%s' " .
      "WHERE subscriber = '%s'";
  $results = db_query($sql_select, $subscriber);
  while ($row = db_fetch_object($results)) {
    $new = str_replace($subscriber, $hostname, $row->subscriber);
    $result = db_query($sql_update, $new, $row->subscriber);
    if (!$result) {
      $successful = FALSE;
    }
  }

  if ($successful) {
    drush_log('Done.', 'success');
  }
  else {
    drush_log('Failed.', 'error');
  }
}

/**
 * Implementation of drush_hook_COMMAND for subscription-switch.
 */
function drush_syndicator_subscription_switch($subscriber, $hostname) {
  $successful = TRUE;

  // Change feeds_push_subscriptions
  $sql_select = "SELECT * FROM {feeds_push_subscriptions} " .
      "WHERE hub LIKE '%%%s%%' " .
      "OR topic LIKE '%%%s%%'";
  $sql_update = "UPDATE {feeds_push_subscriptions} " .
      "SET hub = '%s', topic = '%s' " .
      "WHERE domain = '%s' " .
      "AND subscriber_id = %d";
  $results = db_query($sql_select, $subscriber, $subscriber);
  while ($row = db_fetch_object($results)) {
    $new_hub = str_replace($subscriber, $hostname, $row->hub);
    $new_topic = str_replace($subscriber, $hostname, $row->topic);
    $result = db_query($sql_update, $new_hub, $new_topic, $row->domain, $row->subscriber_id);
    if (!$result) {
      $successful = FALSE;
    }
  }

  // Change feeds_source
  $sql_select = "SELECT * FROM {feeds_source} " .
      "WHERE config LIKE '%%%s%%' " .
      "OR source LIKE '%%%s%%'";
  $sql_update = "UPDATE {feeds_source} " .
      "SET config = '%s', source = '%s' " .
      "WHERE id = %d " .
      "AND feed_nid = %d";
  $results = db_query($sql_select, $subscriber, $subscriber);
  while ($row = db_fetch_object($results)) {
    $new_config = unserialize($row->config);
    $new_config['FeedsHTTPFetcher']['source'] = str_replace($subscriber, $hostname, $new_config['FeedsHTTPFetcher']['source']);
    $new_config = serialize($new_config);
    $new_source = str_replace($subscriber, $hostname, $row->source);
    $result = db_query($sql_update, $new_config, $new_source, $row->id, $row->feed_nid);
    if (!$result) {
      $successful = FALSE;
    }
  }

  // Change feeds_node_item
  $sql_select = "SELECT * FROM {feeds_node_item} " .
      "WHERE url LIKE '%%%s%%' " .
      "OR guid LIKE '%%%s%%'";
  $sql_update = "UPDATE {feeds_node_item} " .
      "SET url = '%s', guid = '%s' " .
      "WHERE nid = %d ";
  $results = db_query($sql_select, $subscriber, $subscriber);
  while ($row = db_fetch_object($results)) {
    $new_url = str_replace($subscriber, $hostname, $row->url);
    $new_guid = str_replace($subscriber, $hostname, $row->guid);
    $result = db_query($sql_update, $new_url, $new_guid, $row->nid);
    if (!$result) {
      $successful = FALSE;
    }
  }

  if ($successful) {
    drush_log('Done.', 'success');
  }
  else {
    drush_log('Failed.', 'error');
  }
}

/**
 * Implementation of drush_hook_COMMAND for topic-switch.
 */
function drush_syndicator_topic_switch($hostname) {
  $successful = TRUE;

  // Change push_hub_subscriptions
  $sql_select = "SELECT * FROM {push_hub_subscriptions}";
  $sql_update = "UPDATE {push_hub_subscriptions} " .
      "SET topic = '%s' " .
      "WHERE subscriber = '%s'";
  $results = db_query($sql_select, $hostname);
  while ($row = db_fetch_object($results)) {
    // http_build_url doesn't exist on all systems
    if (!function_exists('http_build_url')) {
      module_load_include('inc', 'syndicator', 'includes/http_build_url');
    }

    // Changes all topic hostnames no matter what the original is
    $topic_parts = parse_url($row->topic);
    $topic_parts['host'] = $hostname;
    $new_topic = http_build_url('', $topic_parts);
    $result = db_query($sql_update, $new_topic, $row->subscriber);
  }

  if ($successful) {
    drush_log('Done.', 'success');
  }
  else {
    drush_log('Failed.', 'error');
  }
}

/**
 * @}
 */
