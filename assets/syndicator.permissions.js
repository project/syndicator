Drupal.behaviors.janepratt_syndicator = function (context){
  initPermissions();
}

function initPermissions() {
  $('.disabled-field').block({message: null});
  $('.disabled-field').css('visibility', 'visible');
}
