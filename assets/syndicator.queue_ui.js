Drupal.behaviors.janepratt_syndicator = function (context) {
  initScheduleForm();
  initSyndicatorAction();
}

function initScheduleForm() {
  if ($(".syndicator-schedule-submit").length) {
    $(".syndicator-schedule-submit").live('click', function(ev) {
      ev.preventDefault();
      var form = $(this).parent('div.fieldset-content');
      nid = form.find('input[name="nid"]').val();
      workflow = form.find('input[name="workflow"]:checked').val();
      workflow_scheduled = form.find('input[name="workflow_scheduled"]:checked').val();

      scheduled_year = form.find('select[name="workflow_scheduled_date[year]"]').val();
      scheduled_month = form.find('select[name="workflow_scheduled_date[month]"]').val();
      scheduled_day = form.find('select[name="workflow_scheduled_date[day]"]').val();

      workflow_scheduled_hour = form.find('input[name="workflow_scheduled_hour"]').val();

      url = '/syndicator/accept/' + nid;

      form.find(".result").remove();
      form.find(".views-throbbing").remove();
      form.append('<span class="views-throbbing">&nbsp</span>');

      /* Send the data using post and put the results in a div */
      $.post( url, {
            'nid': nid,
            'workflow' : workflow,
            'workflow_scheduled' : workflow_scheduled,
            'workflow_scheduled_date[year]' : scheduled_year,
            'workflow_scheduled_date[month]' : scheduled_month,
            'workflow_scheduled_date[day]' : scheduled_day,
            'workflow_scheduled_hour' : workflow_scheduled_hour
          }, function( data ) {
            console.log(data);
            form.find(".views-throbbing").remove();
            form.find(".result").remove();
            var content = $('<div>' + data.message + '</div>' ).html();
            form.prepend('<div class="result">' + content + '</div>');
            if (data.status == 'true') {
              form.parent('fieldset').find('legend a').text('Scheduled');
              form.parents('tr').removeClass('warning');
              form.parents('tr').addClass('scheduled');
            }
            else {
              form.parent('fieldset').find('legend a').text('Schedule');
              form.parents('tr').removeClass('scheduled');
              form.parents('tr').addClass('warning');
            }
      }, "json");
    });
  }
}

function initSyndicatorAction() {
    $("td.views-field-syndicator-reject > a, td.views-field-syndicator-revert > a").live('click', function(ev) {
        ev.preventDefault();
        var _this = $(this);
        var url = $(this).attr("href");
        var reject_txt = Drupal.t('Reject');
        var revert_txt = Drupal.t('Revert');

        _this.parent().find(".views-throbbing").remove();
        _this.append('<span class="views-throbbing">&nbsp</span>');

        $.post(url, {}, function(data) {
            _this.parent().find(".views-throbbing").remove();
            if (_this.html() == reject_txt) {
                _this.parents('tr').removeClass('warning error scheduled').addClass('error');
                _this.parents('tr').find('.views-field-syndicator-schedule *').attr('disabled', true);
                _this.attr("href", url.replace('reject','revert'));
                _this.html(revert_txt);
            }
            elseif (_this.html() == revert_txt) {
                _this.parents('tr').removeClass('warning error scheduled');
                _this.parents('tr').find('.views-field-syndicator-schedule *').attr('disabled', false);
                _this.attr("href", url.replace('revert','reject'));
                _this.html(reject_txt);
            }

        }, "json");
    });
}
