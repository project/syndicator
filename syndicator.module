<?php

/**
 * @file
 *   General logic that applies to both PuSH publishing and consuming
 */

include_once('includes/syndicator.d6tod7.inc');
/**
 * Implements hook_menu().
 */
function syndicator_menu() {
  module_load_include('inc', 'syndicator', 'includes/syndicator.publish');
  module_load_include('inc', 'syndicator', 'includes/syndicator.queue_ui');
  module_load_include('inc', 'syndicator', 'includes/syndicator.permissions');

  $items = array();
  syndicator_publish_menu($items);
  syndicator_queue_ui_menu($items);
  syndicator_permissions_menu($items);

  $items['syndicator/example.xml'] = array(
    'title' => 'Example feed',
    'page callback' => 'syndicator_example_feed_page',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  $items['syndicator/example/listener'] = array(
    'title' => 'Example listener',
    'page callback' => 'syndicator_example_listener_page',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_node_presave() from Drupal 7().
 */
function syndicator_node_presave($node) {
  module_load_include('inc', 'syndicator', 'includes/syndicator.consume');

  syndicator_consume_node_presave($node);
}

/**
 * Implements hook_node_insert() from Drupal 7().
 */
function syndicator_node_insert($node) {
  module_load_include('inc', 'syndicator', 'includes/syndicator.publish');
  module_load_include('inc', 'syndicator', 'includes/syndicator.consume');

  syndicator_publish_node_insert($node);
  syndicator_consume_node_insert($node);
}

/**
 * Implements hook_node_update() from Drupal 7().
 */
function syndicator_node_update($node) {
  module_load_include('inc', 'syndicator', 'includes/syndicator.publish');
  module_load_include('inc', 'syndicator', 'includes/syndicator.consume');

  syndicator_publish_node_update($node);
  syndicator_consume_node_update($node);
}

/**
 * Implements hook_node_load() from Drupal 7().
 */
function syndicator_node_load($node) {
  module_load_include('inc', 'syndicator', 'includes/syndicator.publish');
  module_load_include('inc', 'syndicator', 'includes/syndicator.consume');

  syndicator_publish_node_load($node);
  syndicator_consume_node_load($node);
}

/**
 * Implements hook_node_delete() from Drupal 7().
 */
function syndicator_node_delete($node) {
  module_load_include('inc', 'syndicator', 'includes/syndicator.consume');

  syndicator_consume_node_delete($node);
}

/**
 * Implements hook_form_alter().
 */
function syndicator_form_alter(&$form, $form_state, $form_id) {
  module_load_include('inc', 'syndicator', 'includes/syndicator.publish');
  module_load_include('inc', 'syndicator', 'includes/syndicator.consume');
  module_load_include('inc', 'syndicator', 'includes/syndicator.permissions');

  syndicator_publish_form_alter($form, $form_state, $form_id);
  syndicator_consume_form_alter($form, $form_state, $form_id);
  syndicator_permissions_form_alter($form, $form_state, $form_id);
}

/**
 * Implements hook_permission() from Drupal 7().
 */
function syndicator_permission() {
  module_load_include('inc', 'syndicator', 'includes/syndicator.permissions');

  $permissions = array();
  syndicator_permissions_permission($permissions);
  return $permissions;
}

/**
 * Implementation of hook_feeds_atom_rdf_map_alter().
 */
function syndicator_feeds_atom_rdf_map_alter(&$target_item, $source_item, $source) {
  module_load_include('inc', 'syndicator', 'includes/syndicator.consume');

  syndicator_consume_feeds_atom_rdf_map_alter($target_item, $source_item, $source);
}

/**
 * Implements hook_theme().
 */
function syndicator_theme($existing, $type, $theme, $path) {
  return array(
    'syndicator_example_feed' => array(
      'arguments' => array(),
      'file' => 'syndicator.theme.inc',
      'path' => drupal_get_path('module', 'syndicator') . '/theme',
      'template' => 'syndicator_example_feed',
    ),
  );
}

/**
 * Implements hook_views_api().
 */
function syndicator_views_api() {
  return array(
    'api' => 2, // D7 Upgrade: Value is 3
    'path' => drupal_get_path('module', 'syndicator') . '/includes/views',
    'template path' => drupal_get_path('module', 'syndicator') . 'theme',
  );
}

/**
 * Implements hook_ctools_plugin_api().
 */
function syndicator_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array(
      'version' => 1,
      'path' => drupal_get_path('module', 'syndicator') . '/includes/feeds',
    );
  }
}

/**
 * Page callback for rendering a feed
 */
function syndicator_example_feed_page() {
  drupal_set_header('Content-Type: application/rss+xml; charset=utf-8');
  print theme('syndicator_example_feed');
}
/**
 * Page callback for an example listener.  Writes to file everything received.
 */
function syndicator_example_listener_page() {
  $data = serialize($_POST) . serialize($_GET);
  file_save_data($data, 'syndicator_listener_results.txt');
  print_r($_POST);
  print_r($_GET);
}
